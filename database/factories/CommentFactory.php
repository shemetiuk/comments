<?php

use Faker\Generator as Faker;

/**
 * @param Faker $faker
 *
 * @return array
 */
$factory->define(App\Comment::class, function (Faker $faker) {
	return [
		'user_name' => $faker->firstName,
		'body'      => $faker->paragraph
	];
});