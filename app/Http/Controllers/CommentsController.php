<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
	/**
	 * @return mixed
	 */
	public function index() {
		$comments = Comment::where('parent_id', 0)
		                   ->orderBy('created_at', 'asc')
		                   ->with('children.children.children')
		                   ->get()
		                   ->toArray();

		return $comments;
	}

	/**
	 * @param Request $request
	 * @param Comment $comment
	 *
	 * @return Comment
	 */
	public function store(Request $request, Comment $comment) {

		$this->validate($request, [
			'user_name' => 'required|max:100',
			'body' => 'required'
		]);

		$data = $this->sanitize($request->all());

		return $comment->create($data);
    }

	/**
	 * @param $data
	 *
	 * @return mixed
	 */
	public function sanitize($data)
	{
		$data['user_name'] = filter_var($data['user_name'], FILTER_SANITIZE_STRING);
		$data['body'] = filter_var($data['body'], FILTER_SANITIZE_STRING);

		return $data;
	}
}