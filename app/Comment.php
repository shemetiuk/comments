<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
		'parent_id', 'level', 'user_name', 'body'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children() {
		return $this->hasMany('App\Comment', 'parent_id', 'id')
		            ->orderBy('created_at', 'asc');
	}

	public function scopeGetNestedComments($query) {
		return $query->orderBy('level', 'desc')->get();
	}
}