<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Comment;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentTest extends TestCase {

	use DatabaseTransactions;

	/**
	 * @test
	 */
	function it_fetches_nested_comments() {

		factory(Comment::class)->create();

		factory(Comment::class)->create([
			'parent_id' => 1,
			'level' => 2
		]);

		factory(Comment::class)->create([
			'parent_id' => 2,
			'level' => 3
		]);

		$last_level_comment = factory(Comment::class)->create([
			'parent_id' => 3,
			'level' => 4
		]);

		$comments = Comment::getNestedComments();

		$this->assertEquals($last_level_comment->id, $comments->first()->id);

		$this->assertCount(4, $comments);
	}
}