<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Comments</title>

    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app">
    <comments></comments>
</div>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
